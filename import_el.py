#!/usr/bin/env python

import pandas as pd
from elasticsearch import Elasticsearch

# Read in CSV file to pandas DataFrame
# convert nan to 0
filename = "2023-05-28T2101.csv"
df = ( pd.read_csv(filename).fillna(0) )
print (df)

# Create a mapping for ElasticSearch
mapvalues = {}
for col in df.columns:
  if col not in ["date", "testname", "filetype"]:
    mapvalues[col] = {"type": "float"}

mappings = {
  "properties": {
    "date": {"type": "text", "analyzer": "standard"},
    "testname": {"type": "text", "analyzer": "standard"},
    "filetype": {"type": "text", "analyzer": "standard"},
  }
}

for key, val in mapvalues.items():
  mappings["properties"][key] = val

print (mappings)

# Connect to ElasticSearch
es = Elasticsearch("http://localhost:9200")
es.info().body

# Create mapping in ElasticSearch
es.indices.create(index="trftestsart", mappings=mappings)

# Feed the DataFrames via mapping into ElasticSearch
columnNames = list(df.columns[1:])
doc = {}
for i, row in df.iterrows():
    for name in columnNames:
        doc[name] = row[name]
        
    #print (doc)
    es.index(index="trftestsart", id=i, document=doc)

# Check how many values are stored
es.indices.refresh(index="trftestsart")
print (es.cat.count(index="trftestsart", format="json"))

# Create a simple ElasticSearch query
resp = es.search(
    index="trftestsart",
    query={
            "bool": {
                "must": {
                    "match_phrase": {
                        "testname": "test_trf_q449_phys_physlite_ca",
                    }
                },
                 "filter": {"bool": {"must": {"match_phrase": {"filetype": "PHYSLITE"}}}},
            },
        },            
)
print (resp.body)


