#!/bin/bash

rm -rf tarball_PandaJob_*
rm *.png

datestamp=`date -d "yesterday 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-2 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-3 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-4 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-5 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-6 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-7 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-8 day 21:01" '+%Y-%m-%dT%H%M'`

arch="x86_64-el9-gcc13-opt"
#arch="x86_64-centos7-gcc11-opt"
#arch="x86_64-centos7-clang14-opt"
#arch="x86_64-centos7-clang15-opt"

echo "Using $datestamp and $arch"
python parse_xaod_arttest_el.py --date $datestamp --arch $arch

