# Parse TrfTestsARTPlots

## Getting started

* Creates plots from the payload.stdout logs of TrfTestsARTPlots https://bigpanda.cern.ch/art/tasks/?package=TrfTestsARTPlots&branch=master/Athena/x86_64-centos7-gcc11-opt&nlastnightlies=7&view=packages that are stored on EOS at /eos/atlas/atlascerngroupdisk/data-art/grid-output/master/Athena/x86_64-centos7-gcc11-opt/
* Use it with:
```
./parse_xaod_arttest.sh 
```
which will create plots of the last days nightly in the local directory and will copy them to $HOME/www/plots/
* Test plots are currently available at https://elmsheus.web.cern.ch/elmsheus/plots/plots.html
 
