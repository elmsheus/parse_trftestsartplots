#!/usr/bin/env python

import os, glob, sys
import argparse
import tarfile
import pandas as pd
import matplotlib.pyplot as plt

import atlas

def payload_files(members):
  for tarinfo in members:
    if os.path.basename(tarinfo.name) == "payload.stdout":
      yield tarinfo

def readfile(filename):

  # Read line into lines array
  with open(filename) as f:
    lines = f.readlines()

  # Set some variable to start values
  iline = 0
  domaindict1 = {}
  domaindict2 = {}
  domaindict3 = {}

  ncontainer1 = ncontainer2 = 0
  count1 = count2 = count3 = count4 = 0
  containerlist1 = []
  containerlist2 = []
  unknown1 = []
  unknown2 = []
  filetype = ""

  testName = ''

  # Loop through all lines
  for line in lines:

    # Determine filetype to parse
    if line.startswith("		ART job name:"):
      testName = os.path.splitext(line.strip().split()[3])[0]
    # Determine filetype to parse
    if line.startswith("============ checkxAOD tmp.AOD"):
      filetype="AOD"
    if line.startswith("============ checkxAOD AOD"):
      filetype="AOD"
    if line.startswith("============ checkxAOD myAOD"):
      filetype="AOD"
    if "1000events_mc21_ttbar.AOD.pool.root" in line:
      filetype="AOD"
    if line.startswith("============ checkxAOD DAOD_PHYS.art.pool.root"):
      filetype="PHYS"
    if line.startswith("============ checkxAOD DAOD_PHYSLITE.art.pool.root"):
      filetype="PHYSLITE"

    # Parse CSV lines with domain sizes
    if line.startswith("CSV"):
      domainlist = lines[iline+1].strip().split(",")
      domainsize = lines[iline+2].strip().split(",")
      domaindict = dict(zip(domainlist, domainsize))

      if filetype == "AOD":
        domaindict1 = domaindict
      if filetype == "PHYS":
        domaindict2 = domaindict 
      if filetype == "PHYSLITE":
        domaindict3 = domaindict 

    iline = iline + 1

  return testName, domaindict1, domaindict2, domaindict3

def parseFiles(dateStamp, dirName):

  tarFiles = glob.glob(dirName+"/*/*log.tar")

  # Loop over the 6 TrfTestARTPlots per day and extract payload.stdout
  for tarFile in tarFiles:
    tar = tarfile.open(tarFile)
    tar.extractall(members=payload_files(tar))

  payloadFiles = glob.glob('tarball_*/payload.stdout')
  payloadFiles.sort(key=os.path.getmtime)

  totalDict = {}

  # Loop over the payload.stdout files
  # Create empty pandas.DataFrame
  df = pd.DataFrame()
  for payload in payloadFiles:
    # Parse the PanDA payload.txt logfile of TrfTestART test 
    testName, dict1, dict2, dict3 = readfile(payload)
    print ("-----")
    print (dateStamp, testName, dirName)

    if testName == "test_trf_citest_phys_physlite_compare":
      print ("Skipping %s", testName)
      continue

    # Plot the AOD, PHYS and PHYSLITE pie charts
    filetypes = [ 'AOD', 'PHYS', 'PHYSLITE']
    alldicts = [ dict1, dict2, dict3 ]

    # Loop over filetypes
    for k in range(0,3):
      d = { "date": dateStamp,
            "testname" : testName,
            "filetype" : filetypes[k],
      }
      for key, value in alldicts[k].items():
        d[key] = float(value)

      # Store d dict in DataFrame
      #df = df.append(d, ignore_index=True)
      df = pd.concat([df, pd.DataFrame.from_records([d])])

      #print (filetypes[k], alldicts[k])
      #print (d)

  print (df)

  # Store DataFrame to CSV file
  df.to_csv(dateStamp+".csv")

  # Create a mapping for elastic
  mapvalues = {}
  for col in df.columns:
    if col not in ["date", "testname", "filetype"]:
      mapvalues[col] = {"type": "integer"}

  mappings = {
    "properties": {
      "date": {"type": "text", "analyzer": "standard"},
      "testname": {"type": "text", "analyzer": "standard"},
      "filetype": {"type": "text", "analyzer": "standard"},
    }
  }

  for key, val in mapvalues.items():
    mappings["properties"][key] = val

  #print (mappings)

  return

def main():

  parser = argparse.ArgumentParser(description="Extracts payload.stdout files of TrfTestsARTPlots logfile archives stored on EOS")
  parser.add_argument("--date", type=str, help="date stamp of test, e.g. 2022-11-14T2101", default="2022-11-14T2101", action="store")
  parser.add_argument("--arch", type=str, help="architecture of the test, x86_64-centos7-gcc11-opt", default="x86_64-centos7-gcc11-opt", action="store")

  args = parser.parse_args()

  if len(sys.argv) < 2:
    parser.print_help()
    sys.exit(1)

  # Directory on EOS wher TrfTestARTPlots logs are stored
  dirName = '/eos/atlas/atlascerngroupdisk/data-art/grid-output/main/Athena/%s/%s/TrfTestsARTPlots/' %(args.arch, args.date)

  # Setup ATLAS style
  atlas.set_atlas()

  parseFiles(args.date, dirName)

if __name__ == "__main__":
  main()
