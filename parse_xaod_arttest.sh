#!/bin/bash

rm -rf tarball_PandaJob_*
rm *.png

datestamp=`date -d "yesterday 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-2 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-3 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-4 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-5 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-6 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-7 day 21:01" '+%Y-%m-%dT%H%M'`
#datestamp=`date -d "-8 day 21:01" '+%Y-%m-%dT%H%M'`

arch="x86_64-el9-gcc13-opt"
#arch="x86_64-centos7-gcc11-opt"
#arch="x86_64-centos7-clang14-opt"
#arch="x86_64-centos7-clang15-opt"

echo "Using $datestamp and $arch"
python parse_xaod_arttest.py --date $datestamp --arch $arch

cp index.html index.html.orig
sed "s/TrfTestsARTPlots/TrfTestsARTPlots ${datestamp}/g" index.html > index.html.patched
#awk -v n=12 -v s="    <li><a href=\"${datestamp}\">${datestamp}</a></li>" 'NR == n {print s} {print}' plots.html > plots.html.patched

eosdir=/eos/user/e/elmsheus
wwwdir=$eosdir/www/plots/$datestamp
mkdir $wwwdir
cp *.png $wwwdir
cp index.html.patched $wwwdir/index.html
cp stylesheet.css $wwwdir

cp plots.html plots.html.patched
for dir in $eosdir/www/plots/*/
do
  dir=${dir%*/}
  datedir="${dir##*/}"
  awk -v n=12 -v s="    <li><a href=\"${datedir}\">${datedir}</a></li>" 'NR == n {print s} {print}' plots.html.patched > plots.html.new
  cp plots.html.new plots.html.patched
done
cp plots.html.patched $eosdir/www/plots/plots.html
